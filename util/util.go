package util

import (
	"merithub/msutil/formatter"
	"net/http"

	"github.com/valyala/fasthttp"
)

type Error struct {
	State   int         `json:"state"`
	Code    string      `json:"code"`
	Message interface{} `json:"message"`
}

type loanState struct {
	PENDING  string
	APPROVED string
	PAID     string
}

var LoanState = loanState{
	PENDING:  "PN",
	APPROVED: "AP",
	PAID:     "PD",
}

func JSONResponse(ctx *fasthttp.RequestCtx, data []byte, customErr *Error) error {

	ctx.Response.Header.SetContentType("application/json")

	if customErr != nil {
		ctx.Response.SetStatusCode(customErr.State)
		errBytes, err := formatter.JSONEncode(customErr)
		ctx.Response.SetBody(errBytes)
		return err
	}
	ctx.Response.SetStatusCode(http.StatusOK)
	ctx.Response.SetBody(data)
	return nil
}

// SuccessResponse ...
func SuccessResponse(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.SetContentType("application/json")
	ctx.Response.SetStatusCode(http.StatusOK)
	data := []byte(`{"message":"success"}`)
	ctx.Response.SetBody(data)

}
