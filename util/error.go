package util

func GetError(state int, code, message string) *Error {

	err := Error{
		State:   state,
		Code:    code,
		Message: message,
	}

	return &err

}
