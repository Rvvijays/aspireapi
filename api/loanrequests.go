package api

import (
	"Rvvijays/aspireapi/db"
	"Rvvijays/aspireapi/util"
	"encoding/json"
	"fmt"

	"github.com/rs/xid"
	"github.com/valyala/fasthttp"
)

type request struct {
	Amount int `json:"amount"`
	Term   int `json:"term"`
}

//  LoanRequest users can request loan
func LoanRequest(ctx *fasthttp.RequestCtx) {

	userId, _ := ctx.UserValue("userId").(string)

	userXid, err := xid.FromString(userId)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "invalidUserId", err.Error()))

		return
	}
	requestData := request{}

	err = json.Unmarshal(ctx.PostBody(), &requestData)

	if err != nil {
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))
		return
	}

	err1 := validateRequest(requestData)

	if err1 != nil {
		util.JSONResponse(ctx, nil, err1)
		return
	}

	laanLoanId := xid.New()

	key := db.LoanKey{
		LoanId: laanLoanId.String(),
	}

	value := db.LoanValue{
		UserId: userXid.String(),
		Amount: requestData.Amount,
		Term:   requestData.Term,
		State:  util.LoanState.PENDING,
	}
	// add in request load database...

	err = db.PutItem(db.LoanDB, key, value)

	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err1.Code))

		return
	}

	// adding in usersloadn database............

	usersLoanKey := db.UsersLoanKey{
		UserId: userXid.String(),
		LoanId: laanLoanId.String(),
	}

	repayments := generateRepayments(laanLoanId, requestData.Amount, requestData.Term)

	userLoanvalue := db.UsersLoanValue{
		Repayments: repayments,
	}

	db.PutItem(db.UsersLoanDB, usersLoanKey, userLoanvalue)

	resp := make(map[string]interface{})

	resp["amount"] = requestData.Amount
	resp["term"] = requestData.Term
	resp["state"] = value.State
	resp["loanId"] = laanLoanId

	resp["repayments"] = repayments

	respBytes, err := json.Marshal(resp)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err1.Code))

		return
	}

	util.JSONResponse(ctx, respBytes, nil)

	// util.SuccessResponse(ctx)

}

func generateRepayments(loanId xid.ID, amount, term int) []db.Repayement {

	repayments := []db.Repayement{}

	for i := 0; i < term; i++ {

		repayment := db.Repayement{
			State:   util.LoanState.PENDING,
			Amount:  amount / term,
			DueDate: loanId.Time().AddDate(0, 0, 7*i).Format("2006-02-01"),
		}

		repayments = append(repayments, repayment)
	}

	return repayments

}

func validateRequest(req request) *util.Error {

	if req.Amount <= 0 {
		return util.GetError(400, "amountNotValid", "loan for requested amount is not valid")
	}

	if req.Term <= 0 {
		return util.GetError(400, "termNotValid", "loan for requested term is not valid")

	}

	return nil

}
