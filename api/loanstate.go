package api

import (
	"Rvvijays/aspireapi/db"
	"Rvvijays/aspireapi/util"
	"encoding/json"
	"fmt"

	"github.com/rs/xid"
	"github.com/valyala/fasthttp"
)

// LoanState this endpoint is used to get state and repayments of the loan of a user
func LoanState(ctx *fasthttp.RequestCtx) {

	userId, _ := ctx.UserValue("userId").(string)
	loanId, _ := ctx.UserValue("loanId").(string)

	userXid, err := xid.FromString(userId)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "invalidUserId", err.Error()))
		return
	}

	loanXid, err := xid.FromString(loanId)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "invalidLoanId", err.Error()))

		return
	}

	usersLonkey := db.UsersLoanKey{
		UserId: userXid.String(),
		LoanId: loanXid.String(),
	}

	loanKey := db.LoanKey{
		LoanId: loanXid.String(),
	}

	val, err := db.GetValue(db.UsersLoanDB, usersLonkey)
	if err != nil {
		fmt.Println("ERr", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))

		return
	}

	if len(val) == 0 {
		util.JSONResponse(ctx, nil, util.GetError(404, "notFound", "loan not found in our database by this loanId."))
		return
	}

	userLoan := db.UsersLoanValue{}

	err = json.Unmarshal(val, &userLoan)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

		return
	}

	val2, err := db.GetValue(db.LoanDB, loanKey)
	if err != nil {
		fmt.Println("ERr", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))
		return
	}

	// fmt.Println("value...............", string(val2))

	loanValue := db.LoanValue{}

	err = json.Unmarshal(val2, &loanValue)
	if err != nil {
		fmt.Println("err22222222222222222222", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

		return
	}

	resp := make(map[string]interface{})

	resp["loanId"] = loanId
	resp["amount"] = loanValue.Amount
	resp["term"] = loanValue.Term
	resp["state"] = loanValue.State

	resp["repayements"] = userLoan.Repayments

	respBytes, err := json.Marshal(resp)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

		return
	}

	util.JSONResponse(ctx, respBytes, nil)

}
