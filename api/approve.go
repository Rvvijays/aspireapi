package api

import (
	"Rvvijays/aspireapi/db"
	"Rvvijays/aspireapi/util"
	"encoding/json"
	"fmt"

	"github.com/valyala/fasthttp"
)

type approveRequest struct {
	LoanId string `json:"loanId"`
}

func ApproveLoan(ctx *fasthttp.RequestCtx) {

	// state :=

	body := approveRequest{}

	err := json.Unmarshal(ctx.PostBody(), &body)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))
		return
	}

	// fmt.Println("loadnId............", body.LoanId)

	key := db.LoanKey{
		LoanId: body.LoanId,
	}

	value, err := db.GetValue(db.LoanDB, key)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))
		return
	}

	loanValue := db.LoanValue{}

	err = json.Unmarshal(value, &loanValue)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))
		return
	}

	if loanValue.State == util.LoanState.APPROVED {
		fmt.Println("alressy approved.....")
		util.JSONResponse(ctx, nil, util.GetError(200, "loanAlreadyApproved", "loan with this loanId is already approved."))
		return
	}

	loanValue.State = util.LoanState.APPROVED

	err = db.PutItem(db.LoanDB, key, loanValue)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))
		return
	}

	util.SuccessResponse(ctx)

}
