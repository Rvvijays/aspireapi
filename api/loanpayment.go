package api

import (
	"Rvvijays/aspireapi/db"
	"Rvvijays/aspireapi/util"
	"encoding/json"
	"fmt"
	"time"

	"github.com/rs/xid"
	"github.com/valyala/fasthttp"
)

type payentReq struct {
	Amount int `json:"amount"`
}

func LoanPayment(ctx *fasthttp.RequestCtx) {

	userId, _ := ctx.UserValue("userId").(string)
	userXid, err := xid.FromString(userId)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "invalidUserId", err.Error()))

		return
	}

	loanId, _ := ctx.UserValue("loanId").(string)

	loanXid, err := xid.FromString(loanId)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "invalidLoanId", err.Error()))

		return
	}

	loanKey := db.LoanKey{
		LoanId: loanXid.String(),
	}

	loanBytes, err := db.GetValue(db.LoanDB, loanKey)

	if err != nil {
		fmt.Println(err)
		return
	}

	loanValue := db.LoanValue{}

	err = json.Unmarshal(loanBytes, &loanValue)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

		return
	}

	if loanValue.State == util.LoanState.PENDING {

		fmt.Println("loan is not approved")
		util.JSONResponse(ctx, nil, util.GetError(200, "notApproved", "loan not approved."))

		return
	}

	if loanValue.State == util.LoanState.PAID {

		fmt.Println("loan is already paid")
		util.JSONResponse(ctx, nil, util.GetError(200, "alreadyPaid", "loan is already paid."))

		return
	}

	body := payentReq{}

	err = json.Unmarshal(ctx.PostBody(), &body)

	if body.Amount <= 0 {
		fmt.Println("amount can't be zero")
		util.JSONResponse(ctx, nil, util.GetError(400, "badRequest", "amount can't be less than 0"))

		return
	}

	userLoanKey := db.UsersLoanKey{
		UserId: userXid.String(),
		LoanId: loanXid.String(),
	}

	val, err := db.GetValue(db.UsersLoanDB, userLoanKey)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))

		return
	}

	userLoanValue := db.UsersLoanValue{}

	err = json.Unmarshal(val, &userLoanValue)
	if err != nil {
		util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

		return
	}

	extra := 0

	paid := true
	repayAccepted := false
	for i, repayment := range userLoanValue.Repayments {

		if !repayAccepted && repayment.State == util.LoanState.PENDING && body.Amount >= repayment.Amount-repayment.PaidAmount {
			repayment.State = util.LoanState.PAID

			repayAccepted = true
			repayment.PaidDate = time.Now().UTC().Format("2006-02-01")
			// body.Amount = 0
			userLoanValue.Repayments[i] = repayment
			repayment.PaidAmount = repayment.Amount

			extra = body.Amount - (repayment.Amount - repayment.PaidAmount)
		}

		if repayment.State == util.LoanState.PENDING {
			if repayAccepted && extra > 0 {
				repayment.PaidAmount += extra
				userLoanValue.Repayments[i] = repayment
				extra = 0
			}
			paid = false
			break
		}
	}

	err = db.PutItem(db.UsersLoanDB, userLoanKey, userLoanValue)
	if err != nil {
		fmt.Println("ERr", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))

		return
	}

	if paid {
		loanValue.State = util.LoanState.PAID

		err = db.PutItem(db.LoanDB, loanKey, loanValue)
		if err != nil {
			fmt.Println("Err", err)
			util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))

			return
		}

		if extra > 0 {
			fmt.Println("loan is closed with extra money do something about it..")
		}
	}

	if repayAccepted {
		util.SuccessResponse(ctx)
		return
	}

	// fmt.Println("amount is not specified")

	util.JSONResponse(ctx, nil, util.GetError(400, "invalidAmount", "amout is less than scheduled payment."))

}
