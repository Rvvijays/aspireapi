package api

import (
	"Rvvijays/aspireapi/db"
	"Rvvijays/aspireapi/util"
	"encoding/json"
	"fmt"

	"github.com/rs/xid"
	"github.com/valyala/fasthttp"
)

func LoansList(ctx *fasthttp.RequestCtx) {

	list, err := db.Scan(db.LoanDB, 100)
	if err != nil {
		fmt.Println("err", err)
		util.JSONResponse(ctx, nil, util.GetError(500, "internalServerError", err.Error()))

		return
	}

	// resp := make(map[string]interface{})

	requests := []interface{}{}

	for _, item := range list {

		key := db.LoanKey{}
		value := db.LoanValue{}

		err = json.Unmarshal(item.Key, &key)
		if err != nil {
			fmt.Println("err", err)
			// util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

			continue
		}

		err = json.Unmarshal(item.Value, &value)
		if err != nil {
			fmt.Println("err", err)
			// util.JSONResponse(ctx, nil, util.GetError(400, "jsonError", err.Error()))

			continue
		}

		loan := make(map[string]interface{})

		loan["loanId"] = key.LoanId
		loan["userId"] = value.UserId
		loan["amount"] = value.Amount
		loan["term"] = value.Term
		loan["state"] = value.State

		id, err := xid.FromString(key.LoanId)
		if err != nil {
			fmt.Println("err", err)
			// util.JSONResponse(ctx, nil, util.GetError(400, "invalidLoanId", err.Error()))

			continue
		}

		loan["creationTime"] = id.Time().UTC().String()

		requests = append(requests, loan)

	}

	resp := make(map[string]interface{})

	resp["loans"] = requests

	respBytes, err := json.Marshal(resp)
	if err != nil {
		fmt.Println("Err", err)
		util.JSONResponse(ctx, nil, util.GetError(400, "invalidLoanId", err.Error()))

		return
	}

	util.JSONResponse(ctx, respBytes, nil)
}
