package api

import (
	"Rvvijays/aspireapi/util"
	"encoding/json"
	"fmt"

	"github.com/rs/xid"
	"github.com/valyala/fasthttp"
)

func GetuserId(ctx *fasthttp.RequestCtx) {

	userId := xid.New().String()

	resp := make(map[string]interface{})

	resp["userId"] = userId

	respBytes, err := json.Marshal(resp)
	if err != nil {
		fmt.Println("err", err)
		return
	}

	util.JSONResponse(ctx, respBytes, nil)
}
