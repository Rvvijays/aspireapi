# aspireapi


## Name
Mini Aspire API

## Description
Let people request for loans and get repayment schedule after approval by admin they can repay their loan according to repayment schedule.
This is only the backend i.e only rest endpoints. In this project I have used badger database ("github.com/dgraph-io/badger") which is key value database. Fasthttp ("github.com/valyala/fasthttp") for router ("github.com/buaazp/fasthttprouter).

## Installation
just run the exe which is already compiled with port 8080.

or to run locally
golang should be installed on you local system.
use command "go run main.go"


## Usage
Endpoints:

1. Get dummy userId 
    In this project I have used the xid package ("github.com/rs/xid") of golang for random id. so all ids used in this project is an xid.

2. Request for loan with amount and term
    Users can request for loan using their userId, loan amount and term

3. View all loans
    Admin can see all loan with their state and user who requested it.

4. Approve loan
    TAdmin can approve the loan using the loanId.

5. Check loan state and repayments schedule
    Using this endpoint user can get loan state and all the repayment schedules.

6. Loan repayment
    Users can repay their loan. loan amount must be greater or equal to the repayment schedule. If extra money is paid then that money will be advanced payment for the next repayment schedule.

## Rest API requests
    Rest api requests are given in endpoints.json;

