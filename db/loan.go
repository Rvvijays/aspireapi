package db

import (
	"fmt"
	"log"

	"github.com/dgraph-io/badger"
)

var LoanDB *badger.DB
var err error

type LoanKey struct {
	LoanId string `json:"LoanId"`
}

type LoanValue struct {
	UserId string `json:"userId"`
	State  string `json:"state"`
	Amount int    `json:"amount"`
	Term   int    `json:"term"`
}

func OpenLoanDB() {
	LoanDB, err = badger.Open(badger.DefaultOptions("aspireapidb/loandb"))
	if err != nil {
		fmt.Println("error while opening loan request db")
		log.Fatal(err)
	}
	// defer db.Close()
}
