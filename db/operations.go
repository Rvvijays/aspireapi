package db

import (
	"encoding/json"
	"fmt"

	"github.com/dgraph-io/badger"
)

func PutItem(db *badger.DB, key, value interface{}) error {

	keyBytes, err := json.Marshal(key)
	if err != nil {
		fmt.Println("error while encoding key")
		return err
	}
	ValueBytes, err := json.Marshal(value)
	if err != nil {
		fmt.Println("error while encoding key")
		return err
	}

	err = db.Update(func(txn *badger.Txn) error {
		return txn.Set(keyBytes, ValueBytes)
	})

	return err

}

func Delete(db *badger.DB, key interface{}) error {

	keyBytes, err := json.Marshal(key)
	if err != nil {
		fmt.Println("error while encoding key")
		return err
	}

	err = db.Update(func(txn *badger.Txn) error {
		return txn.Delete(keyBytes)

	})

	return err
}

type Item struct {
	Key   []byte `json:"key"`
	Value []byte `json:"value"`
}

func Scan(db *badger.DB, limit int) ([]Item, error) {
	items := []Item{}

	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = limit

		it := txn.NewIterator(opts)
		defer it.Close()

		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()

			foundItem := Item{}

			foundItem.Key = item.Key()

			value := []byte{}

			err = item.Value(func(val []byte) error {
				value = val
				return nil
			})

			foundItem.Value = value

			items = append(items, foundItem)
		}

		return err
	})

	return items, err
}

func GetValue(db *badger.DB, key interface{}) ([]byte, error) {
	keyBytes, err := json.Marshal(key)
	if err != nil {
		fmt.Println("error while encoding key")
		return nil, err
	}

	var value []byte

	err = db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(keyBytes)

		if err != nil {
			return err
		}

		err = item.Value(func(val []byte) error {
			value = val
			return nil
		})

		return err
	})

	return value, err

}
