package db

import (
	"fmt"
	"log"

	"github.com/dgraph-io/badger"
)

var UsersLoanDB *badger.DB

type UsersLoanKey struct {
	UserId string `json:"userId"`
	LoanId string `json:"LoanId"`
}

type Repayement struct {
	State      string `json:"state"`
	Amount     int    `json:"amount"`
	DueDate    string `json:"dueDate"`
	PaidDate   string `json:"paidDate"`
	PaidAmount int    `json:"paidAmount"`
}

type UsersLoanValue struct {
	Repayments []Repayement `json:"repayments"`
}

func OpenUsersLoanDB() {
	UsersLoanDB, err = badger.Open(badger.DefaultOptions("aspireapidb/usersloandb"))
	if err != nil {
		fmt.Println("error while opening usersloan db")
		log.Fatal(err)
	}
	// defer db.Close()
}
