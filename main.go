package main

import (
	"Rvvijays/aspireapi/api"
	"Rvvijays/aspireapi/db"
	"log"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

func init() {
	db.Init()
}

func main() {

	// used fasthttp as a router
	router := fasthttprouter.New()

	// dummy userId
	router.GET("/user", api.GetuserId)

	// request loan
	router.POST("/loan/:userId", api.LoanRequest)

	// loan state by particular user
	router.GET("/loan/:userId/:loanId", api.LoanState)

	// loan repayment by user
	router.POST("/loan/:userId/:loanId", api.LoanPayment)

	// all loans
	router.GET("/loans", api.LoansList)

	// approve loan
	router.POST("/approve", api.ApproveLoan)


	// router.GET("/cleardatabase",api.clearDatabase)

	log.Fatal(fasthttp.ListenAndServe(":8080", router.Handler))

}
